FROM phusion/passenger-ruby26:1.0.9

ENV HOME /root

USER $USER:$USER

CMD ["/sbin/my_init"]

RUN apt-get update && \
    apt-get install -y \
    ghostscript && \
    DEBIAN_FRONTEND=nointeractive apt install -y tzdata && \
    apt-get clean && rm -rf /var/lib/apt/lists* /tmp/* /var/tmp/*

RUN gem install bundler

WORKDIR /app

COPY Gemfile Gemfile.lock ./
RUN bundle install

COPY . .