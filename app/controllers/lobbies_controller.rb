class LobbiesController < ApplicationController
  def index
    render json: Lobby.all
  end

  def show
    render json: Lobby.find(params.require(:id))
  end
  
  #entrada
  # {lobby: { name: 'a', pass: 'b' } }
  #saida
  # { name: 'a', pass: 'b'}
  def create
    Lobby.create!(params.require(:lobby).permit(:name, :pass)), status: 201
  end
end
