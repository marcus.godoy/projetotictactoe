Rails.application.routes.draw do
  resources :lobbies
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
