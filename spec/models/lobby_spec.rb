require 'rails_helper'

RSpec.describe Lobby, type: :model do
  describe '.create!' do
    context 'with empty name' do
      it 'raise validation exception' do
        expect(Lobby.create!(pass: '123')).to raise_exceprion
      end
    end

    context 'with name' do
      it 'success' do
        expect(Lobby.create!(name: 'lobby 1')).to eq(true)
      end
    end
  end
end
