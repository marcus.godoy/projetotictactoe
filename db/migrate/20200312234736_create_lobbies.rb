class CreateLobbies < ActiveRecord::Migration[5.2]
  def change
    create_table :lobbies do |t|
      t.string :name
      t.string :pass

      t.timestamps
    end
  end
end
